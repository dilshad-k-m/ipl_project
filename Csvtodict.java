import java.io.*;
import java.time.Year;
import java.util.*;

public class Csvtodict {
    public static void main(String[] args) {
        String file = "/home/dilshad/java/iplProject/matches.csv";

        List<Matches> records = new ArrayList<>();
        String line;
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null) {
                String[] row = line.split(",");
                // System.out.println(Arrays.toString(row));
                Matches m = new Matches(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8],
                        row[9], row[10], row[11], row[12], row[13], row[14]);

                records.add(m);

            }
            br.close();

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        List<Deliveries> drecords = new ArrayList<>();
        String line2;
        String file2 = "/home/dilshad/java/iplProject/deliveries.csv";
        try {
            BufferedReader brr = new BufferedReader(new FileReader(file2));
            {
                while ((line2 = brr.readLine()) != null) {
                    String[] rows = line2.split(",");
                    Deliveries d = new Deliveries(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6],
                            rows[7], rows[8], rows[9], rows[10], rows[11], rows[12], rows[13], rows[14], rows[15],
                            rows[16], rows[17]);
                    drecords.add(d);
                }
                brr.close();
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        // Problem1: Number of matches played per year
        HashMap<String, Integer> x = new HashMap<>();
        for (Matches i : records) {
            if (x.containsKey(i.season)) {
                x.put(i.season, x.get(i.season) + 1);
            } else {
                x.put(i.season, 1);
            }
        }
        // System.out.println(x);
        // Problem2: Number of matches won of all team over all of years of ipl
        HashMap<String, Integer> y = new HashMap<>();
        for (Matches i : records) {
            if (y.containsKey(i.winner)) {
                y.put(i.winner, y.get(i.winner) + 1);
            } else {
                y.put(i.winner, 1);
            }
        }
        // System.out.println(y);
        // Problem3: for the year 2016 get the extra runs conceded per team
        HashSet<String> z = new HashSet<>();
        HashMap<String, Integer> extra = new HashMap<>();
        for (Matches i : records) {
            if (i.season.equals("2016")) {
                z.add(i.id);
            }
        }
        for (Deliveries i : drecords) {
            if (z.contains(i.match_id)) {
                if (extra.containsKey(i.bowling_team)) {
                    extra.put(i.bowling_team, extra.get(i.bowling_team) + Integer.parseInt(i.extra_runs));
                } else {
                    extra.put(i.bowling_team, Integer.parseInt(i.extra_runs));
                }
            }
        }
        // System.out.println(extra);
        // Problem4: For the year 2015 get the top economical bowlers
        HashSet<String> q = new HashSet<>();
        HashMap<String,Double> r= new HashMap<>();
        HashMap<String, Double> p = new HashMap<>();
        HashMap<String, Double> s=new HashMap<>();
        for (Matches i : records) {
            if (i.season.equals("2015")) {
                q.add(i.id);
            }
        }
        for (Deliveries i : drecords) {
            if (q.contains(i.match_id)) {
                Double a = Double.parseDouble(i.total_runs) - Double.parseDouble(i.bye_runs) - Double.parseDouble(i.legbye_runs);
                if (p.containsKey(i.bowler)) {
                    p.put(i.bowler, p.get(i.bowler) + a);
                } else {
                    p.put(i.bowler, a);
                }
                if(r.containsKey(i.bowler)){
                    r.put(i.bowler,r.get(i.bowler)+1.0/6);
                }
                else{
                    r.put(i.bowler,1.0/6);
                    }
            }
        }
        for(Map.Entry i:p.entrySet()){
            String b=(String)i.getKey();
            Double runs=(Double)i.getValue();
            Double eco=runs/(Double)r.get(b);
            s.put(b,eco);
        }
        //s.entrySet().stream().sorted(Map.Entry.comparingByValue()).forEach(System.out::println);
        //Problem 5:Number of toss won by each team of all season
        HashMap<String,Integer> t=new HashMap<>();
        for(Matches i:records){
            if(t.containsKey(i.toss_winner)){
                t.put(i.toss_winner, t.get(i.toss_winner)+1);
            }
            else{
                t.put(i.toss_winner,1);
            }
        }
        System.out.println(t);
    }
}